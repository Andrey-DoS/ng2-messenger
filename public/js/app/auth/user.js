"use strict";
var User = (function () {
    function User(email, password, firstName, lastName) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return User;
}());
exports.User = User;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImF1dGgvdXNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFDSSxjQUFvQixLQUFhLEVBQ2IsUUFBZ0IsRUFDaEIsU0FBa0IsRUFDbEIsUUFBaUI7UUFIakIsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUNiLGFBQVEsR0FBUixRQUFRLENBQVE7UUFDaEIsY0FBUyxHQUFULFNBQVMsQ0FBUztRQUNsQixhQUFRLEdBQVIsUUFBUSxDQUFTO0lBRXJDLENBQUM7SUFDTCxXQUFDO0FBQUQsQ0FQQSxBQU9DLElBQUE7QUFQWSxZQUFJLE9BT2hCLENBQUEiLCJmaWxlIjoiYXV0aC91c2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFVzZXIge1xyXG4gICAgY29uc3RydWN0b3IgKHB1YmxpYyBlbWFpbDogc3RyaW5nLFxyXG4gICAgICAgICAgICAgICAgIHB1YmxpYyBwYXNzd29yZDogc3RyaW5nLFxyXG4gICAgICAgICAgICAgICAgIHB1YmxpYyBmaXJzdE5hbWU/OiBzdHJpbmcsXHJcbiAgICAgICAgICAgICAgICAgcHVibGljIGxhc3ROYW1lPzogc3RyaW5nKSB7XHJcblxyXG4gICAgfVxyXG59Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
